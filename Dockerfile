FROM ubma/ocr-fileformat

RUN set -e -o xtrace \
  ; apk add --allow-untrusted parallel \
  ; parallel --will-cite